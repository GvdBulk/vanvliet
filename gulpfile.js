var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');
var imagemin = require('gulp-imagemin');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var browserSync = require('browser-sync').create();
var del = require('del');

var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var cssnano = require('cssnano');

//Howto dist URL: mklink /J C:\xampp\htdocs\<PROJECT> C:\Users\gertj\Documents\Projects\Git\<PROJECT>\production

var paths = {
  src: 'source/**/*',
  srcPHP: 'source/site/**/*.php',
  srcYML: 'source/site/**/*.yml',
  srcSCSS: 'source/scss/**/*.scss',
  srcMainSCSS: 'source/scss/app.scss',
  srcVendorJS: 'source/scripts/vendor/*.js',
  srcJS: 'source/scripts/*.js',
  srcAssets: 'source/assets/**/*',
  srcImage: 'source/assets/images/*',
  dist: 'production/',
  distURL: 'localhost/vanvliet'
};

gulp.task('styles', function() {
  var processors = [autoprefixer()];

  var minProcessors = [autoprefixer(), cssnano];

  return gulp
    .src(paths.srcMainSCSS)
    .pipe(plumber())
    .pipe(sass())
    .pipe(postcss(processors))
    .pipe(gulp.dest(paths.dist + 'assets/css'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(postcss(minProcessors))
    .pipe(gulp.dest(paths.dist + 'assets/css'))
    .pipe(browserSync.stream());
});

gulp.task('js-vendor', function() {
  return gulp
    .src([
      'node_modules/jquery/dist/jquery.js',
      'node_modules/bootstrap/dist/js/bootstrap.bundle.js',
      'node_modules/jquery-lazy/jquery.lazy.js',
      paths.srcVendorJS
    ])
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest(paths.dist + 'assets/js'))
    .pipe(rename('vendor.min.js'))
    .pipe(
      uglify().on('error', function(e) {
        console.log(e);
      })
    )
    .pipe(gulp.dest(paths.dist + 'assets/js'));
});

gulp.task('js', function() {
  return gulp.src(paths.srcJS).pipe(gulp.dest(paths.dist + 'assets/js'));
});

gulp.task('php', function() {
  return gulp.src(paths.srcPHP).pipe(gulp.dest(paths.dist + 'site'));
});

gulp.task('yml', function() {
  return gulp.src(paths.srcYML).pipe(gulp.dest(paths.dist + 'site'));
});

gulp.task('images', function() {
  return gulp
    .src(paths.srcImage)
    .pipe(
      imagemin(
        [
          imagemin.gifsicle({ interlaced: true }),
          imagemin.jpegtran({ progressive: true }),
          imagemin.optipng({ optimizationLevel: 5 })
        ],
        {
          verbose: true
        }
      )
    )
    .pipe(gulp.dest(paths.srcImage));
});

gulp.task('assets', function() {
  return gulp.src(paths.srcAssets).pipe(gulp.dest(paths.dist + 'assets'));
});

gulp.task('clean', function() {
  return del(
    [
      paths.dist + 'assets/css/*',
      paths.dist + 'assets/js/*',
      paths.dist + 'site/blueprints/*',
      paths.dist + 'site/snippets/*',
      paths.dist + 'site/templates/*'
    ],
    {
      force: true
    }
  );
});

gulp.task('default', ['clean'], function() {
  gulp.start('styles', 'js', 'js-vendor', 'php', 'yml', 'assets');
});

/* WATCH */

gulp.task('watch', function() {
  gulp.watch(paths.srcSCSS, ['styles']);
  gulp.watch(paths.srcJS, ['js']).on('change', function(event) {
    browserSync.reload();
  });
  gulp.watch(paths.srcPHP, ['php']).on('change', function(event) {
    browserSync.reload();
  });
  gulp.watch(paths.srcYML, ['yml']).on('change', function(event) {
    browserSync.reload();
  });
});

gulp.task('serve', function() {
  browserSync.init({
    proxy: paths.distURL,
    open: true,
    notify: false,
    options: {
      reloadDelay: 250,
      cors: true
    }
  });

  gulp.start('watch');
});
