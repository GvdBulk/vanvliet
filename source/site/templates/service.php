<?php
snippet('header');
snippet('menu');
?>

<section id="service" class="text-color-blue">
  <?php if ($page->has('textBackgroundImage') && $page->textBackgroundImage()->toFile()) :?>
  <div class="text-bg-image__image">
    <img src="<?= $page->textBackgroundImage()->toFile()->url() ?>"
      alt="<?= $page->textBackgroundImage()->toFile()->name() ?>">
  </div>
  <?php endif ?>
  <?php snippet('text-background-image'); ?>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 text-center ">
        <div class="cta">
          <h2 class="cta__heading">Een <?= $page->ctaProf() ?> nodig?</h2>
          <p class="cta__sub">Bel of app voor een afspraak</p>
        </div>
      </div>
    </div>
  </div>
  <?php snippet('hexagons-contacts', ['hexagons' => $page->chosenProfiles()->toPages()]); ?>
  <?php snippet('image-fullwidth'); ?>
</section>



<?php
snippet('footer');
?>