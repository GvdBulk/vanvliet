<?php
  snippet('header');
  snippet('menu');
?>

<section id="textpage">
  <?php if ($page->has('textBackgroundImage') && $page->textBackgroundImage()->toFile()) :?>
  <div class="text-bg-image__image">
    <img src="<?= $page->textBackgroundImage()->toFile()->url() ?>"
      alt="<?= $page->textBackgroundImage()->toFile()->name() ?>">
  </div>
  <?php endif ?>
  <?php snippet('text-background-image'); ?>
</section>

<?php
  snippet('footer');
?>