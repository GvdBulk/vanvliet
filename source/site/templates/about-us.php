<?php
snippet('header');
snippet('menu');
?>

<div id='about-us'>
  <?php snippet('image-text') ?>
  <?php snippet('text-special') ?>
  <?php $profiles = $pages->filterBy('intendedTemplate', 'contact')->children(); ?>
  <?php snippet('hexagons', ['hexagons' => $profiles, 'animated' => true]) ?>
</div>

<?php
snippet('footer');
?>