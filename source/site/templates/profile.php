<?php
  snippet('header');
  snippet('menu');
?>

<div id="profile">
  <?php snippet('image-text') ?>
  <?php snippet('text-background-image') ?>
  <?php snippet('hexagons-contacts', ['hexagons' => array($page)]); ?>
</div>

<?php
  snippet('footer');
?>