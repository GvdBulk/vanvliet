<?php
$activeItem = rand(0, $page->testimonials()->toStructure()->count() - 1);
?>

<div id="carousel" class="carousel slide testimonials" data-ride="carousel">
  <h2 class="heading-primary text-center">Wat vinden ze van ons?</h2>
  <div class="stars">
    <img src="<?= $site->homepage()->url() ?>/assets/images/stars.png" alt="google-review-stars">
  </div>
  <div class="container carousel-inner">
    <div class="row justify-content-center">
      <div class="col-12 col-lg-10">
        <?php foreach ($page->testimonials()->toStructure() as $key => $slide): ?>
        <?php if ($slide->testimonialShow()->isTrue()): ?>
        <div class="carousel-item testimonials__item <?= $key == $activeItem ? 'active' : ''  ?>">
          <p class="testimonials__paragraph"><?= $slide->testimonialText() ?></p>
          <p class="testimonials__author"><?= $slide->testimonialAuthor() ?></p>
        </div>
        <?php endif ?>
        <?php endforeach ?>
      </div>
    </div>

  </div>
  <ol class="carousel-indicators">
    <?php foreach ($page->testimonials()->toStructure() as $key => $slide): ?>
    <li data-target="#carousel" data-slide-to="<?= $key ?>"
      class="testimonials__breadcrump <?= $key == $activeItem ? 'active' : ''  ?>"></li>
    <?php endforeach ?>
  </ol>
  <a class="testimonials__button testimonials__button--prev" href="#carousel" role="button" data-slide="prev">
    <span class="sr-only">Previous</span>
  </a>
  <a class="testimonials__button testimonials__button--next" href="#carousel" role="button" data-slide="next">
    <span class="sr-only">Next</span>
  </a>
</div>
<div class="reviews">
  <a class='button-link' href="<?= $site->reviewLink() ?>" target="_blank">
    <div class="button button-blue">
      Lees meer reviews
    </div>
  </a>
</div>