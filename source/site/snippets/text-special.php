<section class="text-special">
  <div class="container">
    <div class="row">
      <?php $specialTexts = $page->specialTexts()->toStructure() ?>
      <?php foreach($specialTexts as $specialText): ?>
      <div class="col-md-<?= (12 / $specialTexts->count()) ?> skill">
        <h1 class="text-center pb-2">
          <?= $specialText->specialTitle() ?>
        </h1>
        <div class="skill__text">
          <?= $specialText->specialText()->kirbytext() ?>
        </div>
      </div>
      <?php endforeach ?>
    </div>
  </div>
</section>