<?php if ($page->fullWidthImageImage()->toFile()) :?>
<img src="<?= $page->fullWidthImageImage()->toFile()->url() ?>"
  alt="<?= $page->fullWidthImageImage()->toFile()->name() ?>" class="image-fullwidth">
<?php endif ?>