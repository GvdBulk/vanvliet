<?php $homepage = $site->homePage(); ?>
<section class='image-text' style="background-image: url('<?= $page->mainImage()->toFile()->url() ?>')">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="image-text__hexagon">
          <div class="image-text__hexagon--inner">
            <img src="<?= $homepage->url() ?>/assets/images/hexagon-green.png" alt="van-vliet-hexagon-green">
            <div class="image-text__hexagon--text">
              <div class="my-auto">
                <?= $page->mainText()->kirbytext(); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>