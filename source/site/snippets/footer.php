<?php $contact = $pages->filterBy('intendedTemplate', 'contact')->first(); ?>
<footer>
  <div class="container">
    <div class="row contact-info text-center text-md-left">
      <div class="col-12 col-md-4 order-1 order-md-1 mb-4 mb-sm-0">
        <h2>Van Vliet Therapie <br>
          Osteopathie en fysiotherapie in Waddinxveen en Nieuwerkerk aan den IJssel</h2>
        <br>
        <p>
          <?php foreach($contact->addresses()->toStructure() as $address): ?>
          <?= $address->name() ?><br>
          <?= $address->addressLine() ?><br>
          <?= $address->addressPostalCode() ?><br>
          <?= $address->addressCity() ?><br>
          <br>
          <?php endforeach ?>
        </p>
      </div>
      <div class="col-12 col-md-4 order-3 order-md-2 text-center">
        <div class="logo">
          <img src="<?=$site->homepage()->url() ?>/assets/images/logo-wit.png" alt="van-vliet-logo-footer">
        </div>
      </div>
      <div class="col-12 col-md-4 order-2 order-md-3 text-center mb-4 mb-sm-0">
        <h2>Contact</h2>
        <br>
        <?php $profiles = $contact->children()->filterBy('poes', false); ?>
        <p>
          <?php foreach($profiles as $profile): ?>
          <a href="mailto:<?= $profile->email() ?>"><?= $profile->email() ?></a><br>
          <?php endforeach ?>
          <br>
          <?php foreach($profiles as $profile): ?>
          <?= $profile->phone()?> (<?= $profile->hexTitle() ?>)<br>
          <?php endforeach ?>
        <p>
      </div>
    </div>
  </div>
  <div class="text-center copyright">
    Copyright <?php echo date("Y"); ?> <a href="http://corinevandenbulk.nl">Corine.</a> & <a
      href="http://gertjanvdbulk.nl">GJ</a>
  </div>
</footer>

<?= js('assets/js/vendor.min.js') ?>
<?= js('assets/js/main.js') ?>