<?php 
    $i = 0; 
    $textColors = array('light-grey', 'blue', 'green', 'pink', 'grey');

?>

<div class="container hexagon-container">
  <div class="row no-gutters">
    <?php foreach($hexagons as $hexagon) : ?>

    <?php 
      $i++;                
      $colClass = $i % 2 !== 0 ? 'col-8 offset-2 col-md-4 offset-md-0' : 'col-8 offset-2 col-md-4 offset-md-2';
      $textboxClass = 'contact__text-box contact__text-box--' . $textColors[$i % 5];                 
    ?>

    <div class="<?= $colClass ?>">
      <figure class="hexagon hexagon-contact">
        <?php if ($hexagon->hexImg()->toFile()) :?>
        <img src="<?= $hexagon->hexImg()->toFile()->url() ?>" alt="<?= $hexagon->hexImg()->toFile()->name() ?>"
          class="hexagon__img">
        <?php endif ?>

      </figure>
    </div>

    <div class="col-12 col-md-4">
      <div class="<?= $textboxClass ?>">
        <h2 class="contact__text"><?= $hexagon->hexTitle() ?></h2>
        <h4 class="contact__subtitle"><?= $hexagon->hexSubTitle() ?></h4>
        <a href='mailto:<?= $hexagon->email() ?>' class="contact__text--sub"><?= $hexagon->email() ?></a>
        <a href='tel:<?= $hexagon->phone() ?>' class="contact__text--sub"><?= $hexagon->phone() ?></a>
      </div>
    </div>
    <div class="offset-2 col-8 offset-md-0 col-md-2" style="position: relative">
      <div class="contact__appointments text-center my-auto">
        <h2 class='d-none d-md-block d-lg-none'>Afspraak maken?</h2>
        <?php foreach($hexagon->appointmentLinks()->toStructure() as $appointmentLink): ?>
        <a class='button-link' href="<?= $appointmentLink->appointmentLink() ?>">
          <div class='button <?= 'button-' . $textColors[$i % 5] ?>'>
            <h2 class='d-block d-md-none d-lg-block'>Afspraak maken </h2>
            <p><?= $appointmentLink->appointmentLocation() ?></p>
          </div>
        </a>
        <?php endforeach ?>
      </div>
    </div>
    <?php endforeach ?>
  </div>
</div>