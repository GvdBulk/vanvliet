<?php $i = 0; 
if (!isset($animated)) {
    $animated = false;
}
?>

<div class="container hexagon-container">
  <div class="row no-gutters">
    <?php foreach($hexagons as $hexagon) : ?>
    <?php if($hexagon->has('hexVisible') && $hexagon->hexVisible()->toBool()): ?>
    <?php 
      $i++;                
      $colClass = $i % 5 !== 4 ? 'col-8 offset-2 col-sm-4 offset-sm-0' : 'col-8 offset-2 col-sm-4 offset-sm-2';
      $hexClass = 'hexagon__text-box hexagon__text-box--' . $hexagon->hexColor(); 
    ?>
    <div class="<?= $colClass ?>">
      <div class="<?= $animated ? 'hexagon hexagon--animated' : 'hexagon'?>">
        <img src="<?= $hexagon->hexImg()->toFile()->url() ?>" alt="<?= $hexagon->hexImg()->toFile()->name() ?>"
          class="hexagon__img">
        <div class="hexagon__transparent">
        </div>
        <div class="<?= $hexClass ?>">
          <h2 class="hexagon__text"><?= $hexagon->hexTitle() ?></h2>
          <a href="<?= $hexagon->url() ?>" class="hexagon__btn">Lees meer</a>
        </div>
      </div>
    </div>
    <?php endif ?>
    <?php endforeach ?>
  </div>
</div>