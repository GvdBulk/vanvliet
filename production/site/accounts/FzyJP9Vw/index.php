<?php

return [
    'email' => 'vanvliet@gertjanvdbulk.nl',
    'language' => 'nl',
    'name' => 'Admin',
    'role' => 'admin'
];