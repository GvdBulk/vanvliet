<?php
snippet('header');
snippet('menu');
?>

<div id='blog'>
  <?php snippet('text-special') ?>
  <?php snippet('hexagons', ['hexagons' => $page->children()]); ?>
</div>



<?php
snippet('footer');
?>