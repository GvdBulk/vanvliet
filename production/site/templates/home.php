<?php
  snippet('header');
  snippet('menu');
?>

<div id="home">
  <?php snippet('image-text') ?>
  <div class="container">
    <div class="row justify-content-center buttons">
      <div class="col-12 col-md-6 col-lg-4">
        <a class='button-link' href="./diensten/fysiotherapie">
          <div class="button button-blue">
            <div class="button-text">
              Voor een afspraak<br>
              FYSIOTHERAPIE <br>
            </div>
          </div>
        </a>
      </div>
      <div class="col-12 col-md-6 col-lg-4">
        <a class='button-link' href="./diensten/osteopathie">
          <div class="button button-blue">
            <div class="button-text">

              Voor een afspraak<br>
              OSTEOPATHIE <br>
              (vanaf 4 jaar)
            </div>
          </div>
        </a>
      </div>
      <div class="col-12 col-md-6 col-lg-4">
        <a class='button-link' href="tel:0031638050509">
          <div class="button button-blue">
            <div class="button-text">
              Afspraak wijzigen / verzetten<br>
              Bel +31 (0)6-38050509<br>
            </div>
          </div>
        </a>
      </div>
    </div>
  </div>
  <?php snippet('text-special') ?>
  <section id="home">
    <div class=" container">
      <div class="row text-center">
        <div class="col-12">
          <h1 class="heading-primary">Diensten</h1>
        </div>
      </div>
    </div>
    <?php
      $services = $pages->filterBy('intendedTemplate', 'services')->first();
      snippet('hexagons', ['hexagons' => $services->childrenAndDrafts(), 'animated' => true]);
    ?>
  </section>
  <?php snippet('testimonials'); ?>
  <div class="about-us">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-lg-10">
          <?= $page->aboutUsText()->kirbytext() ?>
          <a class='button-link' href="./over-ons">
            <div class="button button-blue">
              Lees meer over ons
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
  snippet('footer');
?>