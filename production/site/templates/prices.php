<?php
  snippet('header');
  snippet('menu');
?>

<div class="prices-special">
  <?php snippet('text-special') ?>
  <div class="container">
    <div class="row justify-content-center">

      <?php foreach($page->pricesContent()->toStructure() as $priceContent): ?>

      <div class="col-12 col-md-6 col-lg-4">
        <a class='button-link' href="<?= $priceContent->link()->url()?>" target='_blank'>
          <div class="button button-green" style='text-transform: uppercase'>
            Vergoeding <?= $priceContent->mainTitle() ?>
          </div>
        </a>
      </div>
      <?php endforeach ?>
    </div>
  </div>
</div>
<section id="prices">
  <div class="container ">
    <?php foreach($page->pricesContent()->toStructure() as $priceContent): ?>
    <div class="row mb-5">
      <div class="col-12">
        <h1>Tarieven <?= $priceContent->mainTitle() ?></h1>
      </div>
      <?php foreach($priceContent->prices()->toStructure() as $price): ?>
      <div class="col-12 col-md-4 my-3">
        <div class="price">
          <h2 class="price__title"><?= $price->priceTitle() ?></h2>
          <hr />
          <div class="price__actualprice text-center">
            <?= $price->price() ?>
          </div>
          <div class="price__description"><strong><?= $price->priceDescription() ?></strong></div>
        </div>
      </div>
      <?php endforeach ?>
    </div>
    <?php endforeach ?>
  </div>
</section>


<?php
  snippet('footer');
?>