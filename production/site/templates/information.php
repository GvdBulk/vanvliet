<?php
  snippet('header');
  snippet('menu');
?>
<?php snippet('text-special') ?>
<section id="information">
  <div class="container">
    <div class="row justify-content-center">
      <?php foreach($page->children() as $child): ?>
      <div class="col-12 col-md-4">
        <a class='button-link' href="<?= $child->url() ?>">
          <div class="button button-green">
            <?= $child->title() ?>
          </div>
        </a>
      </div>
      <?php endforeach ?>
    </div>
  </div>
</section>

<?php
  snippet('footer');
?>