<?php
snippet('header');
snippet('menu');
?>

<?php snippet('text-special'); ?>
<?php snippet('hexagons', ['hexagons' => $page->childrenAndDrafts(), 'animated' => true]); ?>

<?php
snippet('footer');
?>

<?= js('assets/js/carousel.js') ?>