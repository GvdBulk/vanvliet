<?php
  snippet('header');
  snippet('menu');
?>

<div id='contact'>
  <section id='opening-hours'>
    <div class="container">
      <h1>Openingstijden</h1>
      <div class="row">
        <?php $i = 0; ?>
        <?php foreach($page->children()->filterBy('poes', false) as $child): ?>
        <div class="col-12 col-md-6">
          <div class="opening-hours__main <?= $i % 2 == 0 ? 'bg-color-blue' : 'bg-color-green' ?>">
            <div class="opening-hours__header">
              <h1><?= $child->hexTitle() ?></h1>
              <p>
                <a href="mailto:<?= $child->email() ?>"><?= $child->email() ?></a>
              </p>
              <p>
                <a href="tel:<?= $child->phone() ?>"><?= $child->phone() ?></a>
              </p>
            </div>
            <div class="opening-hours__table">
              <?php $openingHours = $child->openingHours()->toStructure(); ?>
              <table>
                <tr>
                  <th></th> <!-- Empty corner header -->
                  <?php foreach($openingHours as $oh): ?>
                  <th><?= $oh->location() ?></th>
                  <?php endforeach ?>
                </tr>
                <tr>
                  <td><strong>MA</strong></td>
                  <?php foreach($openingHours as $oh): ?>
                  <td><?= $oh->days()->toStructure()->findBy('day', 'MA')->availability() ?></td>
                  <?php endforeach ?>
                </tr>
                <tr>
                  <td><strong>DI</strong></td>
                  <?php foreach($openingHours as $oh): ?>
                  <td><?= $oh->days()->toStructure()->findBy('day', 'DI')->availability() ?></td>
                  <?php endforeach ?>
                </tr>
                <tr>
                  <td><strong>WO</strong></td>
                  <?php foreach($openingHours as $oh): ?>
                  <td><?= $oh->days()->toStructure()->findBy('day', 'WO')->availability() ?></td>
                  <?php endforeach ?>
                </tr>
                <tr>
                  <td><strong>DO</strong></td>
                  <?php foreach($openingHours as $oh): ?>
                  <td><?= $oh->days()->toStructure()->findBy('day', 'DO')->availability() ?></td>
                  <?php endforeach ?>
                </tr>
                <tr>
                  <td><strong>VR</strong></td>
                  <?php foreach($openingHours as $oh): ?>
                  <td><?= $oh->days()->toStructure()->findBy('day', 'VR')->availability() ?></td>
                  <?php endforeach ?>
                </tr>
              </table>
            </div>
          </div>
        </div>
        <?php $i++; ?>
        <?php endforeach ?>
      </div>
    </div>
  </section>
  <section id='appointments'>
    <div class="container appointment">
      <div class="row justify-content-center">
        <div class="col-12 col-md-8">
          <div class="appointment__header text-center mb-5">
            <h1><strong>Wilt u direct een afspraak maken?</strong></h1>
            <p>Maak direct een afspraak in onze agenda's</p>
          </div>
        </div>
      </div>
      <div class="row justify-content-center">
        <?php foreach($page->children()->filterBy('poes', false) as $child): ?>
        <div class="col-12 col-md-4 ">
          <div class="appointment__title text-center">
            <h1><strong><?= $child->hexTitle() ?></strong></h1>
            <p><?= $child->hexSubtitle()->split(',')[0] ?></p>
          </div>
          <?php foreach($child->appointmentLinks()->toStructure() as $appointmentLink): ?>
          <a class='button-link' href="<?= $appointmentLink->appointmentLink() ?>">
            <div class="button button-blue">
              <?= $appointmentLink->appointmentLocation() ?>
            </div>
          </a>
          <?php endforeach ?>
        </div>
        <?php endforeach ?>
      </div>
    </div>
  </section>
  <section id='addresses'>
    <div class="container">
      <div class="row justify-content-center">
        <?php foreach($page->addresses()->toStructure() as $address): ?>
        <div class="col-12 col-md-4">
          <div class="address text-center">
            <h1><strong><?= $address->name() ?></strong></h1>
            <p>
              <?= $address->addressLine() ?><br>
              <?= $address->addressPostalCode() . ' ' . $address->addressCity() ?>
            </p>
          </div>
        </div>
        <?php endforeach ?>
      </div>
    </div>
  </section>
  <section id='contact-form'>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-md-8">
          <div class="contact-title text-center mb-5">
            <h1><strong><?= $page->contactFormTitle() ?></strong></h1>
            <h4><?= $page->contactFormSubtitle() ?></h4>
          </div>
          <form id="contact-form" method="post" action="<?= $page->url() ?>">
            <div class="form-group">
              <input type="text" placeholder='Uw naam' class="form-control" id="name" name="name" value="" required>
            </div>
            <div class="form-group">
              <input type="email" placeholder="Email" class="form-control" id="email" name="email" value="">
            </div>
            <div class="form-group">
              <input type="text" placeholder="Telefoonnummer" class="form-control" id="phone" name="phone" value="">
            </div>
            <div class="form-group">
              <textarea id="message" placeholder="Uw bericht" name="message" cols="30" rows="10"
                class="form-control"></textarea>
            </div>
            <?php if($success): ?>
            <div class="">
              <div class="success text-center mt-3">
                <?= $page->successMessage()->kirbytext() ?>
              </div>
            </div>
            <?php elseif(isset($alert['error'])): ?>
            <div class="">
              <div class="error text-center mt-5">
                <?= $alert['error'] ?>
                <?= $page->errorMessage()->kirbytext() ?>
              </div>
            </div>
            <?php else: ?>
            <input type="submit" name="submit" value="Verzend" class="mx-auto">
            <?php endif ?>
        </div>
        </form>
        <div class="home__quote">
          <div class="home__quote-text font-parisienne my-auto text-center">
            <?= $page->contactQuote() ?>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


<?php
  snippet('footer');
?>