<?php
  snippet('header');
  snippet('menu');
?>

<section class="article">
  <?php if ($page->has('textBackgroundImage') && $page->textBackgroundImage()->toFile()) :?>
  <div class="text-bg-image__image">
    <img src="<?= $page->textBackgroundImage()->toFile()->url() ?>"
      alt="<?= $page->textBackgroundImage()->toFile()->name() ?>">
  </div>
  <?php endif ?>
  <?php snippet('text-background-image'); ?>
  <?php snippet('hexagons-contacts', ['hexagons' => $page->chosenProfiles()->toPages()]); ?>
</section>

<?php
  snippet('footer');
?>