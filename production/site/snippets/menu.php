<?php $homepage = $pages->filterBy('intendedTemplate', 'home')->first(); ?>
<nav id="menu" class="navbar navbar-expand-lg navbar-dark nav-bg-black">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <!-- <span class="fas fa-bars"></span> -->
    <img src="<?= $homepage->url() ?>/assets/images/menu icon.png" alt="">
  </button>
  <a class="navbar-brand" href="<?= $homepage->url() ?>">
    <img src="<?= $homepage->url() ?>/assets/images/logo.png" alt="van-vliet-logo">
  </a>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav nav-bg-black mx-auto">
      <?php foreach($pages as $page): ?>
      <?php if($page->content()->has('menuShow') && $page->menuShow()->toBool()): ?>
      <?php if(($page->hasChildren() || $page->hasDrafts()) && ($page->childrenAndDrafts()->filterBy('menuShow', true)->count() > 0)): ?>
      <div class="nav-item dropdown">
        <a class="nav-link dropdown-toggle px-4 px-lg-3 px-xl-3 py-3 py-lg-2 my-auto" href="<?= $page->url() ?>"
          id="navbarDropdownMenuLink" aria-haspopup="true" aria-expanded="false">
          <?= $page->menuText()->kirbytext() ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <?php foreach($page->childrenAndDrafts() as $child): ?>
          <?php if($child->content()->has('menuShow') && $child->menuShow()->toBool()): ?>
          <a class="dropdown-item" href="<?= $child->url() ?>">
            <?php if($child->has('menuImage') && $child->menuImage()->toFile()): ?>
            <img src="<?= $child->menuImage()->toFile()->url() ?>" alt="<?= $child->menuImage()->toFile()->name() ?>">
            <?php endif ?>
            <?= $child->menuText()->kirbytext() ?>
          </a>
          <?php endif ?>
          <?php endforeach ?>
        </div>
      </div>
      <?php else: ?>
      <div class="nav-item">
        <a class="nav-link px-4 px-lg-3 px-xl-3 py-3 py-lg-2 my-auto"
          href="<?= $page->url() ?>"><?= $page->menuText()->kirbytext() ?></a>
      </div>
      <?php endif ?>
      <?php endif ?>
      <?php endforeach ?>
    </div>
  </div>
</nav>