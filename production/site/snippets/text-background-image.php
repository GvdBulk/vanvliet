<div class="container ">
  <div class="row">
    <div class="col-12 text-bg-image__container">
      <h1 class="text-bg-image__title"><?= $page->textTitle() ?></h1>
      <p class="text-bg-image__subtitle"><?= $page->textSubtitle() ?></p>
      <div class="text-bg-image__text">
        <?= $page->textText()->kirbytext() ?>
      </div>
    </div>
  </div>
</div>