<!DOCTYPE html>

<head>
  <meta charset="UTF-8">
  <meta name=viewport content="width=device-width, initial-scale=1">

  <?php
    snippet('metadata');
  ?>

  <!-- Title -->
  <title>
    <?= ($page->content()->has('seoTitle') && !$page->seoTitle()->isEmpty()) ? $page->seoTitle() : $site->seoTitle() ?>
  </title>

  <?= css('https://use.fontawesome.com/releases/v5.2.0/css/all.css', ['integrity' => 'sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ', 'crossorigin' => 'anonymous']) ?>
  <?= css(array(
    'assets/css/app.min.css'
  )) ?>
</head>