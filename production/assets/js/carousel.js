document.addEventListener('DOMContentLoaded', () => {
  const container = document.querySelector('.carousel__content-container');
  const list = container.firstElementChild;
  const slides = Array.from(list.children);

  const prev = document.querySelector('.carousel__button--prev');
  const next = document.querySelector('.carousel__button--next');

  let direction = 'prev';

  const transitionType = getComputedStyle(container).transition;

  // Set initial carousel item (random)
  list.prepend(list.children[getRandomIndex(list.children.length)]);

  // Click listeners for <prev> and <next> button
  prev.addEventListener('click', () => {
    if (direction === 'prev') {
      list.appendChild(list.firstElementChild);
      list.style.justifyContent = 'flex-end';
      direction = 'next';
    }
    container.style.transform = 'translate(100%)';

    // last item is showing
    const lastSlide = list.children[list.children.length - 2];
    lastSlide.style.opacity = 1;

    slides.forEach((slide) => {
      if (slide !== lastSlide) {
        slide.style.opacity = 0;
      }
    });
  });

  next.addEventListener('click', () => {
    if (direction === 'next') {
      list.prepend(list.lastElementChild);
      list.style.justifyContent = 'flex-start';
      direction = 'prev';
    }

    container.style.transform = 'translate(-100%)';
    
    // first item is showing
    const firstSlide = list.children[1];
    firstSlide.style.opacity = 1;

    slides.forEach((slide) => {
      if (slide !== firstSlide) {
        slide.style.opacity = 0;
      }
    });
  });

  // Listeners for transition end
  container.addEventListener('transitionend', () => {
    if (direction === 'prev') {
      list.appendChild(list.firstElementChild);
    } else if (direction === 'next') {
      list.prepend(list.lastElementChild);
    }

    container.style.transition = 'none';
    container.style.transform = 'translate(0)';

    setTimeout(() => {
      container.style.transition = transitionType;
    }, 0);
  });

  Array.from(list.children).forEach((item) => {
    item.addEventListener('transitionend', (ev) => {
      ev.stopImmediatePropagation();
    });
  });
});

const getRandomIndex = (max, min = 1) => {
  return Math.floor(Math.random() * max + (min - 1));
};
