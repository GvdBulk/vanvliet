jQuery(function($) {
  if ($(window).width() > 991) {
    $('.navbar .dropdown')
      .on('mouseenter', function() {
        $(this)
          .find('.dropdown-menu')
          .first()
          .stop(true, true)
          .delay(250)
          .slideDown();
      })
      .on('mouseleave', function() {
        $(this)
          .find('.dropdown-menu')
          .first()
          .stop(true, true)
          .delay(100)
          .slideUp();
      });

    $('.navbar .dropdown > a').on('click', function() {
      location.href = this.href;
    });
  }
});
